//
//  DataOperationTests.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 27/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataOperation.h"
#import "URLSessionMock.h"


@interface DataOperationTests : XCTestCase

@property (nonatomic, strong) NSURL *validUrl;
@property (nonatomic, strong) URLSessionMock *session;

@end

@implementation DataOperationTests

static NSString *const validUrlStr = @"http://abc.com";


- (void)setUp {
    [super setUp];
    _validUrl = [NSURL URLWithString: validUrlStr];
    _session = [[URLSessionMock alloc] init];
}

- (void)tearDown {
    _session = nil;
    [super tearDown];
}

- (void)testErrorIfErrorNotEquealToNil {
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:
                                   @"Test error"};
    NSError *testError = [NSError errorWithDomain:NSURLErrorDomain
                                                code:NSURLErrorUnknown
                                            userInfo: userInfo];
    self.session.error = testError;
    DataOperation *dataOperation = [[DataOperation alloc] initWithUrl:_validUrl successCAllback:^(NSData *data) {
        XCTFail(@"Must not call sucess on error");
    } errorCallback:^(NSError *error) {
        XCTAssertTrue([error.localizedDescription isEqualToString: @"Test error"]);
    }];
    dataOperation.session = self.session;
    [dataOperation main];
    
}

- (void)testUnknownErrorIfDataResponseAndErrorAreNil {
    DataOperation *dataOperation = [[DataOperation alloc] initWithUrl:_validUrl successCAllback:^(NSData *data) {
        XCTFail(@"Must not call sucess on error");
    } errorCallback:^(NSError *error) {
        XCTAssertTrue([error.localizedDescription isEqualToString: @"Unknown error"]);
    }];
    dataOperation.session = self.session;
    [dataOperation main];
    
}

- (void)testHttpErrorStatusCode404 {
    NSInteger httpStatusCode = 404;
    NSData *data = [[NSData alloc] init];
    NSHTTPURLResponse *httpResponse = [[NSHTTPURLResponse alloc] initWithURL:self.validUrl
                                                                  statusCode:httpStatusCode
                                                                 HTTPVersion:nil
                                                                headerFields:nil];
    DataOperation *dataOperation = [[DataOperation alloc] initWithUrl:_validUrl successCAllback:^(NSData *data) {
        XCTFail(@"Must not call sucess on error");
    } errorCallback:^(NSError *error) {
        XCTAssertTrue([error.localizedDescription isEqualToString: @"HTTP request failed with status code 404"]);
    }];
    dataOperation.session = _session;
    _session.data = data;
    _session.response = httpResponse;
    [dataOperation main];
    
}

- (void)testDataOperationSuccess {
    NSInteger httpStatusCode = 200;
    NSData *data = [[NSData alloc] init];
    NSHTTPURLResponse *httpResponse = [[NSHTTPURLResponse alloc] initWithURL:self.validUrl
                                                                  statusCode:httpStatusCode
                                                                 HTTPVersion:nil
                                                                headerFields:nil];
    DataOperation *dataOperation = [[DataOperation alloc] initWithUrl:_validUrl successCAllback:^(NSData *data) {
        XCTAssertNotNil(data);
    } errorCallback:^(NSError *error) {
        XCTFail(@"Must not call error on sucess");
    }];
    dataOperation.session = _session;
    _session.data = data;
    _session.response = httpResponse;
    [dataOperation main];
    
}

@end
