//
//  UrlBuilderTests.m
//  Pixabay
//
//  Created by Andrey Volobuev on 3/13/17.
//  Copyright © 2017 test. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "URLBuilder.h"



@interface UrlBuilderTests : XCTestCase

@property (nonatomic, strong) NSURL *validURL;
@property (nonatomic, strong) URLBuilder *urlBuilder;
@end

@implementation UrlBuilderTests

static NSString *const urlStr = @"https://abc.com/";
static NSString *const key = @"12345";

- (void)setUp {
    [super setUp];
    _validURL = [NSURL URLWithString: urlStr];
    _urlBuilder = [[URLBuilder alloc] initWithUrl:_validURL];
}

- (void)tearDown {

    [super tearDown];
}

- (void)testUrlBulderSearchTermOneWord {
    NSURL * url = [self.urlBuilder withSearchTerms:@"yellow"].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?q=yellow"]]);
}

- (void)testUrlBulderSearchTermTwoWords {
    NSURL * url = [self.urlBuilder withSearchTerms:@"yellow flowers"].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?q=yellow+flowers"]]);
}

- (void)testUrlBulderSearchTermThreeWord {
    NSURL * url = [self.urlBuilder withSearchTerms:@"yellow red flowers"].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?q=yellow+red+flowers"]]);
}

- (void)testSpacesAtTheBeginingd {
    NSURL * url = [self.urlBuilder withSearchTerms:@"   yellow flowers"].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?q=yellow+flowers"]]);
}

- (void)testSpacesAtTheEnd {
    NSURL * url = [self.urlBuilder withSearchTerms:@"yellow flowers    "].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?q=yellow+flowers"]]);
}

- (void)testSpacesAtTheBeginingAndTheEnd {
    NSURL * url = [self.urlBuilder withSearchTerms:@" yellow flowers "].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?q=yellow+flowers"]]);
}

- (void)testSpacesAtTheBeginingAndTheEndOneWord {
    NSURL * url = [self.urlBuilder withSearchTerms:@" yellow "].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?q=yellow"]]);
}

- (void)testKey {
    NSURL * url = [self.urlBuilder withKey:key].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?key=12345"]]);
}

- (void)testKeyAndSearchTerm {
    NSURL * url = [[self.urlBuilder withKey:key] withSearchTerms:@" yellow    flowers "].url;
    XCTAssertTrue([url isEqual: [NSURL URLWithString:@"https://abc.com/?key=12345&q=yellow+flowers"]]);
}

@end
