//
//  URLSessionMock.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 27/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "URLSessionDataTaskMock.h"


@interface URLSessionMock : NSURLSession

@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSURLResponse *response;
@property (nonatomic, strong) NSError *error;

@end
