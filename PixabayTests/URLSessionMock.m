//
//  URLSessionMock.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 27/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "URLSessionMock.h"

@implementation URLSessionMock

-(NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url
                       completionHandler:(void (^)(NSData * _Nullable, NSURLResponse * _Nullable, NSError * _Nullable))completionHandler {

    completionHandler(self.data, self.response, self.error);
    
    return [[URLSessionDataTaskMock alloc] init];
};

@end
