//
//  DetailsViewModel.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"


@interface DetailsViewModel : NSObject

@property (nonatomic, strong, nonnull, readonly) NSString *name;
@property (nonatomic, strong, nonnull, readonly) NSString *tags;
@property (nonatomic, strong, nonnull, readonly) NSString *likes;
@property (nonatomic, strong, nonnull, readonly) NSString *favorites;
@property (nonatomic, strong, nonnull, readonly) NSString *comments;

- (instancetype _Nonnull)initWithItem: (Item * _Nonnull) item;

@end
