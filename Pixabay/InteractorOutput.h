//
//  InteractorOutput.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Item;

@protocol InteractorOutput <NSObject>

-(void)didLoadedImageData: (NSData *) data forUrl: (NSURL *) url;
-(void)errorDidOccured: (NSError *) error;
@optional
-(void)didLoadedItems: (NSArray<Item *> *) items;

@end
