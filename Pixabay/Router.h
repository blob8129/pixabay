//
//  Router.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainTableViewController.h"
#import "Interactor.h"
#import "Presenter.h"
#import "NetworkManager.h"
#import "Item.h"
#import "DetailsRouter.h"

/**
 
 Router is responsible for the navigation and for assembling the module.
 
 */
@interface Router : NSObject

@property (nonatomic, weak, readonly) MainTableViewController *viewController;
@property (nonatomic, weak) UINavigationController *navigationController;

-(void)navigateToDetailsOfItem: (Item *) item;

@end
