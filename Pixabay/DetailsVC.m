//
//  DetailsVC.m
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "DetailsVC.h"


@interface DetailsVC ()
@property (weak, nonatomic) IBOutlet UIImageView *detailsImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;
@property (weak, nonatomic) IBOutlet UILabel *likesLabel;
@property (weak, nonatomic) IBOutlet UILabel *favoritesLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation DetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter viewDidLoad];
}


//MARK: DetailsPresenterOutput protocol

-(void)startActivityIndicator {
    [self.activityIndicator startAnimating];
}

-(void)stopActivityIndicator {
    [self.activityIndicator stopAnimating];
}

-(void)updateViewWithViewModel:(DetailsViewModel *)viewModel {
    self.nameLabel.text = viewModel.name;
    self.tagsLabel.text = viewModel.tags;
    self.likesLabel.text = viewModel.likes;
    self.favoritesLabel.text = viewModel.favorites;
    self.commentsLabel.text = viewModel.comments;
}

-(void)updateViewWithImageData:(NSData *)data {
    self.detailsImageView.image = [UIImage imageWithData:data];
}

@end
