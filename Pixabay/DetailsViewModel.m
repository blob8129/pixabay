//
//  DetailsViewModel.m
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "DetailsViewModel.h"


@implementation DetailsViewModel

- (instancetype)initWithItem: (Item *) item {
    self = [super init];
    if (self) {
        _name = [NSString stringWithFormat:@"User name: %@", item.user];
        _tags = [NSString stringWithFormat:@"Tags: %@", item.tags];
        _likes = [NSString stringWithFormat:@"Likes: %@", item.likes];
        _comments = [NSString stringWithFormat:@"Comments: %@", item.likes];
        _favorites = [NSString stringWithFormat:@"Favorites: %@", item.likes];
    }
    return self;
}

@end
