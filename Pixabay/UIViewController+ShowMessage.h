//
//  UIViewController+ShowMessage.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ShowMessage)

-(void)showMessage:(NSString *)message withTitle:(NSString *)title;

@end
