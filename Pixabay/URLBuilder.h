//
//  URLBuilder.h
//  Pixabay
//
//  Created by Andrey Volobuev on 12/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLBuilder : NSObject
    
@property (nonnull, nonatomic, strong, readonly) NSURL *url;
    
-(URLBuilder * _Nonnull)withKey: (NSString * _Nonnull) key;
-(URLBuilder * _Nonnull)withSearchTerms: (NSString * _Nonnull) searhTerms;
    
-(instancetype _Nonnull)initWithUrl: (NSURL * _Nonnull) url ;
    
@end
