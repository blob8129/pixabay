//
//  MainTableViewController.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewModel.h"
#import "PresenterInput.h"
#import "PresenterOutput.h"

@interface MainTableViewController : UITableViewController <PresenterOutput, UISearchResultsUpdating>

@property (nonatomic, strong) id<PresenterInput> presenter;

@end
