//
//  InteractorInput.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol InteractorInput <NSObject>

-(void)loadItemsForSearchTerm: (NSString*) searchTerm;
-(void)loadImageDataForUrl: (NSURL*) url;
-(void)cancelAll;

@end
