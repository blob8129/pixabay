//
//  Presenter.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainViewModel.h"
#import "PresenterOutput.h"
#import "Router.h"
#import "InteractorOutput.h"
#import "InteractorInput.h"
@class Router;


/**
 
 Presenter deals with presentation logic.
 
 */
@interface Presenter : NSObject <PresenterInput, InteractorOutput>

- (instancetype)initWithInteractor: (id<InteractorInput>) interactor
                              view: (id<PresenterOutput>) view
                            router: (Router *) router;

@end
