//
//  PresenterOutput.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PresenterOutput <NSObject>

-(void)updateView;
-(void)updateViewAtIndex: (NSInteger) index;
@optional
-(void)showMessage: (NSString *) message withTitle: (NSString *) title;

@end
