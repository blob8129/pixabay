//
//  DataOperation.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 23/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "DataOperation.h"


@interface DataOperation ()

@property (nonatomic, strong) void(^successCallback)(NSData *);
@property (nonatomic, strong) void(^errorCallback)(NSError *);

@end

@implementation DataOperation

-(NSURLSession *)session {
    if (_session == nil) {
        _session = [NSURLSession sharedSession];
    }
    return _session;
}

-(void)main {
    
    NSURLSessionTask *task = [self.session dataTaskWithURL: self.url
                                         completionHandler:^(NSData * _Nullable data,
                                                             NSURLResponse * _Nullable response,
                                                             NSError * _Nullable error) {
                                             
                                             if (error != nil) {
                                                 if (self.isCancelled == NO) {
                                                     self.errorCallback(error);
                                                 }
                                                 [self finishOperation];
                                                 return;
                                             }
                                             
                                             if (data == nil && response == nil) {
                                                 NSDictionary *userInfo = @{NSLocalizedDescriptionKey:
                                                                                @"Unknown error"};
                                                 NSError *unknownError = [NSError errorWithDomain:NSURLErrorDomain
                                                                                             code:NSURLErrorUnknown
                                                                                         userInfo: userInfo];
                                      
                                                 if (self.isCancelled == NO) {
                                                     self.errorCallback(unknownError);
                                                 }
                                                 [self finishOperation];
                                                 return;
                                             }

                                             NSInteger httpStatusCode = [(NSHTTPURLResponse *)response statusCode];
                                             
                                             if (httpStatusCode < 200 || httpStatusCode >= 300) {
                                                 
                                                 NSString *description = [NSString stringWithFormat:
                                                                          @"HTTP request failed with status code %zd",
                                                                          httpStatusCode];
                                                 NSDictionary *userInfo = @{NSLocalizedDescriptionKey: description};
                                                 NSError *httpError = [NSError errorWithDomain:NSURLErrorDomain
                                                                                             code:NSURLErrorUnknown
                                                                                         userInfo: userInfo];
                                              
                                                 if (self.isCancelled == NO) {
                                                     self.errorCallback(httpError);
                                                 }
                                                 [self finishOperation];
                                                 return;
                                             }

                                             if (self.isCancelled == NO) {
                                                 self.successCallback(data);
                                             }
                                             [self finishOperation];
                                         }];
    [task resume];
}

- (instancetype)initWithUrl: (NSURL *) url
            successCAllback: (void(^)(NSData *)) successCallback
              errorCallback: (void(^)(NSError *)) errorCallback {
    self = [super init];
    if (self) {
        _url = url;
        _successCallback = successCallback;
        _errorCallback = errorCallback;
    }
    return self;
}

@end
