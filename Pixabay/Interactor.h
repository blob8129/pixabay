//
//  Interactor.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkManager.h"
#import "URLBuilder.h"
#import "InteractorInput.h"
#import "InteractorOutput.h"


/**
 
 Interactor deals with the buissines logic.
 
 */
@interface Interactor : NSObject <InteractorInput>

@property (nonatomic, weak) id<InteractorOutput> presenter;

- (instancetype)initWithNetworkManager: (NetworkManager *) networkManager;

@end
