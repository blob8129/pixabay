//
//  Track.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 23/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Item : NSObject

@property (nonatomic, readonly) NSInteger itemId;
@property (nonatomic, strong, nonnull, readonly) NSString *user;
@property (nonatomic, strong, nonnull, readonly) NSString *tags;
@property (nonatomic, strong, nonnull, readonly) NSURL *previewURL;
@property (nonatomic, strong, nonnull, readonly) NSURL *webformatURL;
@property (nonatomic, strong, nonnull, readonly) NSNumber *likes;
@property (nonatomic, strong, nonnull, readonly) NSNumber *favorites;
@property (nonatomic, strong, nonnull, readonly) NSNumber *comments;

- (instancetype _Nonnull)initWithJson: (NSDictionary * _Nonnull) json;

@end
