//
//  MainViewModel.m
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "MainViewModel.h"

@implementation MainViewModel

- (instancetype _Nonnull)initWithItem: (Item * _Nonnull) item
                            imageData: (NSData * _Nullable) data {
    self = [super init];
    if (self) {
        _name = item.user;
        _tags = item.tags;
        _imageData = data;
    }
    return self;
}

@end
