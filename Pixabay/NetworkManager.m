//
//  NetworkManager.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 23/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "NetworkManager.h"


@interface NetworkManager ()

@property (nonatomic, strong) NSOperationQueue *queue;

@end

@implementation NetworkManager

-(NSURLSession *)session {
    if (_session == nil) {
        _session = [NSURLSession sharedSession];
    }
    return _session;
}

-(NSOperationQueue *)queue {
    if (_queue == nil) {
        _queue = [[NSOperationQueue alloc] init];
    }
    return _queue;
}

-(void)loadDataForURL: (NSURL *) url
              successCallback: (void(^)(NSData*, NSURL*)) successCallback
                errorCallback: (void(^)(NSError*)) errorCallback {
    
    DataOperation *dataOperation = [[DataOperation alloc] initWithUrl:url successCAllback:^(NSData *data) {
        
        successCallback(data, url);
        
    } errorCallback: errorCallback];
    
    [self.queue addOperation:dataOperation];
}

-(void)cancelAll {
    [self.queue cancelAllOperations];
}

@end
