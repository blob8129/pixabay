//
//  Router.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "Router.h"



@implementation Router

-(MainTableViewController *)instatiateViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"MainTVC"];
}

-(void)assembleModule {
    NetworkManager *networkManager = [[NetworkManager alloc] init];
    Interactor *interactor = [[Interactor alloc] initWithNetworkManager:networkManager];
    Presenter *presenter = [[Presenter alloc] initWithInteractor: interactor
                                                            view: _viewController
                                                          router: self];
    interactor.presenter = presenter;
    _viewController.presenter = presenter;
}

-(void)navigateToDetailsOfItem: (Item *) item {
    DetailsRouter *detailsRouter = [[DetailsRouter alloc] initWithItem:item];
    [self.navigationController pushViewController:detailsRouter.viewController animated:YES];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _viewController = [self instatiateViewController];
        [self assembleModule];
    }
    return self;
}

@end
