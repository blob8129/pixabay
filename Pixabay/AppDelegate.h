//
//  AppDelegate.h
//  Pixabay
//
//  Created by Andrey Volobuev on 12/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Router.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

