//
//  DetailsVC.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailsPresenterOutput.h"
#import "DetailsPresenterInput.h"
#import "DetailsViewModel.h"


@interface DetailsVC : UIViewController <DetailsPresenterOutput>

@property (nonatomic, strong) id<DetailsPresenterInput> presenter;

@end
