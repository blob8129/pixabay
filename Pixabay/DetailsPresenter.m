//
//  DetailsPresenter.m
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "DetailsPresenter.h"


@interface DetailsPresenter ()

@property (nonatomic, strong) id<InteractorInput> interactor;
@property (nonatomic, strong) Item *item;
@property (nonatomic, weak) id<DetailsPresenterOutput> view;

@end


@implementation DetailsPresenter

- (instancetype)initWithItem: (Item *) item
                  interactor: (id<InteractorInput>) interactor
                     andView: (id<DetailsPresenterOutput>) view {
    self = [super init];
    if (self) {
        _item = item;
        _interactor = interactor;
        _view = view;
    }
    return self;
}


// MARK: PresenterInput protocol

-(void)viewDidLoad {
    DetailsViewModel *viewModel = [[DetailsViewModel alloc] initWithItem:self.item];
    [self.view updateViewWithViewModel: viewModel];
    [self.interactor loadImageDataForUrl:self.item.webformatURL];
    [self.view startActivityIndicator];
}


// MARK: InteractorOutput protocol

-(void)didLoadedImageData:(NSData *)data forUrl:(NSURL *)url {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view updateViewWithImageData:data];
        [self.view stopActivityIndicator];
    });
}

-(void)errorDidOccured:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view stopActivityIndicator];
        [self.view showMessage:error.localizedDescription withTitle:@"Error occured"];
    });
}

@end
