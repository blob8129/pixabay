//
//  AppDelegate.m
//  Pixabay
//
//  Created by Andrey Volobuev on 12/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    Router *router = [[Router alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] init];
    router.navigationController = navigationController;
    [navigationController pushViewController:router.viewController animated:NO];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
