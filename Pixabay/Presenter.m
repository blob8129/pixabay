//
//  Presenter.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "Presenter.h"


@interface Presenter ()

@property (nonatomic, strong) NSArray<Item*> *items;
@property (nonatomic, strong) NSMutableDictionary<NSURL*, NSNumber*> *urlsIndexes;
@property (nonatomic, strong) NSMutableDictionary<NSNumber*, NSData*> *indexesData;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) id<InteractorInput> interactor;
@property (nonatomic, strong) Router *router;
@property (nonatomic, weak) id<PresenterOutput> view;

@end

@implementation Presenter

static NSString *const initialSearchTerm = @"apples";
static NSTimeInterval const debounceInterval = 1.0;


- (instancetype)initWithInteractor: (id<InteractorInput>) interactor
                              view: (id<PresenterOutput>) view
                            router: (Router *) router {
    self = [super init];
    if (self) {
        _interactor = interactor;
        _view = view;
        _router = router;
    }
    return self;
}


-(void)debounceAndSerchForTerm: (NSString *) searchTerm {
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:debounceInterval
                                                  target:self
                                                selector:@selector(searchTermDebounced:)
                                                userInfo:searchTerm
                                                 repeats:NO];
}

-(void)searchTermDebounced: (NSTimer *) timer {
    if ([timer.userInfo isKindOfClass:[NSString class]]) {
        NSString *searchTerm = timer.userInfo;
        [self.interactor loadItemsForSearchTerm:searchTerm];
    }
}


// MARK: PresenterInput protocol

-(void)viewDidLoad {
    [self.interactor loadItemsForSearchTerm:initialSearchTerm];
}

-(NSInteger)numberOfRows {
    return self.items.count;
}

-(void)didChangedSearchTerm:(NSString *)searchTerm {
    [self.interactor cancelAll];
    if ([searchTerm isEqualToString:@""] == YES) {
        [self.timer invalidate];
        self.items = [[NSArray<Item*> alloc] init];
        self.urlsIndexes = [[NSMutableDictionary<NSURL*, NSNumber*> alloc] init];
        self.indexesData = [[NSMutableDictionary<NSNumber*, NSData*> alloc] init];
        [self.view updateView];
        return;
    }
    [self debounceAndSerchForTerm:searchTerm];

}

-(NSString *)getInitialSearchTerm {
    return initialSearchTerm;
}


-(MainViewModel *)viewMOdelForIndex: (NSInteger) index {
    Item *item = self.items[index];
    NSData *imageData = self.indexesData[[NSNumber numberWithUnsignedInteger:index]];
    return [[MainViewModel alloc] initWithItem:item imageData:imageData];
}


-(void)didSelectedIndex:(NSInteger)index {
    Item *item = self.items[index];
    [self.router navigateToDetailsOfItem:item];
    
}

// MARK: InteractorOutput protocol

-(void)didLoadedItems: (NSArray<Item *> *) items {
    self.items = items;
    self.urlsIndexes = [[NSMutableDictionary<NSURL*, NSNumber*> alloc] init];
    self.indexesData = [[NSMutableDictionary<NSNumber*, NSData*> alloc] init];

    [items enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        self.urlsIndexes[item.previewURL] = [NSNumber numberWithUnsignedInteger:idx];
        [self.interactor loadImageDataForUrl:[item previewURL]];
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view updateView];
    });
}

-(void)didLoadedImageData:(NSData *)data forUrl:(NSURL *)url {
   
    NSInteger index = self.urlsIndexes[url].integerValue;
    self.indexesData[[NSNumber numberWithUnsignedInteger:index]] = data;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view updateViewAtIndex:index];
    });
}

-(void)errorDidOccured:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view showMessage:error.localizedDescription withTitle:@"Error occured"];
    });
}

@end
