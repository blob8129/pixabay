//
//  DetailsPresenter.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InteractorInput.h"
#import "InteractorOutput.h"
#import "DetailsPresenterInput.h"
#import "DetailsPresenterOutput.h"
#import "Item.h"
#import "DetailsViewModel.h"


@interface DetailsPresenter : NSObject <InteractorOutput, DetailsPresenterInput>

- (instancetype)initWithItem: (Item *) item
                  interactor: (id<InteractorInput>) interactor
                     andView: (id<DetailsPresenterOutput>) view;

@end
