//
//  Interactor.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "Interactor.h"


@interface Interactor ()

@property (nonatomic, strong) NetworkManager *networkManager;
@property (nonatomic, strong) NSURL *pixabayURL;


@end

@implementation Interactor

static NSString *const pixabayURLStr = @"https://pixabay.com/api/";
static NSString *const key = @"27347-23fd1708b1c4f768195a5093b";

-(NSURL *)pixabayURL {
    if (_pixabayURL == nil) {
        _pixabayURL = [NSURL URLWithString: pixabayURLStr];        
    }
    return _pixabayURL;
}
- (instancetype)initWithNetworkManager: (NetworkManager *) networkManager {
    self = [super init];
    if (self) {
        _networkManager = networkManager;
    }
    return self;
}

-(void)cancelAll {
    [self.networkManager cancelAll];
}


// MARK: InteractorInput
-(void)loadItemsForSearchTerm: (NSString*) searchTerm {

    URLBuilder *urlBuilder = [[URLBuilder alloc] initWithUrl:self.pixabayURL];
    NSURL * url = [[urlBuilder withKey:key] withSearchTerms:searchTerm].url;

    [self.networkManager loadDataForURL:url successCallback:^(NSData *data, NSURL *url) {
        NSError *serializetionError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData: data
                                                             options: NSJSONReadingMutableContainers
                                                               error: &serializetionError];
        if (serializetionError != nil) {
            [self.presenter errorDidOccured:serializetionError];
            return;
        }
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey:
                                       @"Invalid element in json"};
        NSError *jsonReadError = [NSError errorWithDomain:NSCocoaErrorDomain
                                                     code:NSPropertyListReadCorruptError
                                                 userInfo: userInfo];
        if ([json isKindOfClass:[NSDictionary class]] == false) {
            [self.presenter errorDidOccured:jsonReadError];
            return;
        }
        
        NSMutableArray<Item *> *items = [[NSMutableArray<Item *> alloc] init];
        
        NSArray *hits = json[@"hits"];
        if ([hits isKindOfClass:[NSArray class]] == false) {
            [self.presenter errorDidOccured:jsonReadError];
            return;
        }
        
        [hits enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            Item *item = [[Item alloc] initWithJson: obj];
            [items addObject: item];
        }];
        [self.presenter didLoadedItems:items];
    } errorCallback:^(NSError * error) {
        [self.presenter errorDidOccured:error];
    }];
}


-(void)loadImageDataForUrl: (NSURL*) url {
    [self.networkManager loadDataForURL:url successCallback:^(NSData *data, NSURL *url) {
        [self.presenter didLoadedImageData:data forUrl:url];
    } errorCallback:^(NSError * error) {
        [self.presenter errorDidOccured:error];
    }];
}


@end
