//
//  Track.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 23/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "Item.h"


@implementation Item

- (instancetype)initWithJson: (NSDictionary *) json {
    self = [super init];
    if (self) {
        _itemId =  [json[@"id"] integerValue];
        _user = json[@"user"];
        _tags = json[@"tags"];
        NSString *previewURLstr = json[@"previewURL"];
        _previewURL = [NSURL URLWithString:previewURLstr];
        NSString *webformatURLStr = json[@"webformatURL"];
        _webformatURL = [NSURL URLWithString:webformatURLStr];
        _likes = json[@"likes"];
        _favorites = json[@"favorites"];
        _comments = json[@"comments"];
    }
    return self;
}

@end
