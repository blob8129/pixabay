//
//  URLBuilder.m
//  Pixabay
//
//  Created by Andrey Volobuev on 12/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "URLBuilder.h"


@implementation URLBuilder
  
    
-(NSURL *)withQueries: (NSDictionary<NSString *, NSString *> *) queries {
    NSMutableArray<NSURLQueryItem *> *queryItems = [[NSMutableArray<NSURLQueryItem *> alloc] init];
    [queries.allKeys enumerateObjectsUsingBlock:^(NSString * _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        NSURLQueryItem *queryItem = [NSURLQueryItem queryItemWithName:key value:queries[key]];
        [queryItems addObject:queryItem];
    }];
    
    NSURLComponents *components = [NSURLComponents componentsWithURL:self.url
                                             resolvingAgainstBaseURL:YES];
    if (components.queryItems == nil) {
        components.queryItems = queryItems;
    } else {
        NSArray<NSURLQueryItem *> *appendedItems = [components.queryItems
                                                arrayByAddingObjectsFromArray:queryItems];
        components.queryItems = appendedItems;
    }
    
    if (components.URL == nil) {
        return nil;
    }
    
    return components.URL;
}
    
    
-(URLBuilder *)withKey: (NSString *) key {
    NSURL *url = [self withQueries: @{@"key": key}];
    return [[URLBuilder alloc] initWithUrl:url];
}
    
-(URLBuilder *)withSearchTerms: (NSString *) searhTerms {
    NSCharacterSet *spaceSet = [NSCharacterSet characterSetWithCharactersInString:@" "];
    NSString *trimmedSearchTerm = [searhTerms stringByTrimmingCharactersInSet:spaceSet];
    
    NSString *searchTermsWithPlus = [trimmedSearchTerm stringByReplacingOccurrencesOfString:@"\\s+"
                                                                          withString:@"+"
                                                                             options:NSRegularExpressionSearch
                                                                               range: NSMakeRange(0, trimmedSearchTerm.length)];
    NSURL *url = [self withQueries: @{@"q": searchTermsWithPlus}];
    return [[URLBuilder alloc] initWithUrl:url];
}
    
- (instancetype)initWithUrl: (NSURL *) url {
    self = [super init];
    if (self) {
        _url = url;
    }
    return self;
}
    
@end
