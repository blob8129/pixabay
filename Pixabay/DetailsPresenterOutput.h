//
//  DetailsPresenterOutput.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DetailsViewModel;


@protocol DetailsPresenterOutput <NSObject>

-(void)updateViewWithViewModel: (DetailsViewModel *) viewModel;
-(void)updateViewWithImageData: (NSData *) data;
-(void)startActivityIndicator;
-(void)stopActivityIndicator;
@optional
-(void)showMessage: (NSString *) message withTitle: (NSString *) title;

@end
