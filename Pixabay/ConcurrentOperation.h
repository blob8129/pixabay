//
//  ConcurrentOperatin.h
//  FlickrTest
//
//  Created by Andrey Volobuev on 2/21/16.
//  Copyright © 2016 test. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 
 `ConcurrentOperation` is a subclass of `NSOperation` with the manual state management
 
 */
@interface ConcurrentOperation : NSOperation

-(void)finishOperation;

@end
