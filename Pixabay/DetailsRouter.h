//
//  DetailsRouter.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailsVC.h"
#import "Interactor.h"
#import "Item.h"
#import "DetailsPresenter.h"


@interface DetailsRouter : NSObject

@property (nonatomic, weak, readonly) DetailsVC *viewController;

- (instancetype)initWithItem: (Item *) item;

@end
