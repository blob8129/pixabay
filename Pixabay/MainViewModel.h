//
//  MainViewModel.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

@interface MainViewModel : NSObject

@property (strong, nonatomic, nonnull, readonly) NSString* name;
@property (strong, nonatomic, nonnull, readonly) NSString* tags;
@property (strong, nonatomic, nullable, readonly) NSData* imageData;

- (instancetype _Nonnull)initWithItem: (Item * _Nonnull) item imageData: (NSData * _Nullable) data;

@end
