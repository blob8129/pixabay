//
//  DetailsRouter.m
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "DetailsRouter.h"


@implementation DetailsRouter

-(DetailsVC *)instatiateViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Details" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"DetailsVC"];
}

-(void)assembleModule: (Item *) item {
    NetworkManager *networkManager = [[NetworkManager alloc] init];
    Interactor *interactor = [[Interactor alloc] initWithNetworkManager:networkManager];
    DetailsPresenter *presenter = [[DetailsPresenter alloc] initWithItem: item
                                                              interactor: interactor
                                                                 andView: _viewController];
    interactor.presenter = presenter;
    _viewController.presenter = presenter;
}

- (instancetype)initWithItem: (Item *) item {
    self = [super init];
    if (self) {
        _viewController = [self instatiateViewController];
        [self assembleModule:item];
    }
    return self;
}

@end
