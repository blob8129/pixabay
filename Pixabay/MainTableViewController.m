//
//  MainTableViewController.m
//  SoundCloud
//
//  Created by Andrey Volobuev on 26/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "MainTableViewController.h"


@interface MainTableViewController ()

@property (nonatomic, strong) UISearchController *searchController;

@end


@implementation MainTableViewController

-(UISearchController *)searchController {
    if (_searchController == nil) {
        _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    }
    return _searchController;
}

-(void)setSearch {
    self.searchController.dimsBackgroundDuringPresentation = NO;
    [self.searchController loadViewIfNeeded];
    self.definesPresentationContext = YES;
    self.searchController.searchResultsUpdater = self;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter viewDidLoad];
    [self setSearch];
    self.searchController.searchBar.text = [self.presenter getInitialSearchTerm];
}

- (IBAction)refreshAction:(id)sender {
     [self.presenter didChangedSearchTerm:self.searchController.searchBar.text];;
}


// MARK: Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter numberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"Cell" forIndexPath:indexPath];
    MainViewModel *viewModel = [self.presenter viewMOdelForIndex:indexPath.row];
    cell.textLabel.text = viewModel.name;
    cell.detailTextLabel.text = viewModel.tags;
    cell.imageView.image = [UIImage imageWithData: viewModel.imageData];
    
    return cell;
}


// MARK: Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.presenter didSelectedIndex:indexPath.row];
}


// MARK: Presenter output

-(void)updateView {
    [self.tableView.refreshControl endRefreshing];
    [self.tableView reloadData];
}

-(void)updateViewAtIndex:(NSInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
}


// MARK: Presenter output

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchTerm = searchController.searchBar.text;
    [self.presenter didChangedSearchTerm:searchTerm];
}

@end
