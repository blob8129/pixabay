//
//  DataOperation.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 23/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import "ConcurrentOperation.h"

@protocol URLDataLoader <NSObject>

@property (nonatomic, strong, readonly) NSURL *url;
@property (nonatomic, strong, readonly) NSData *data;
@property (nonatomic, strong, readonly) NSError *error;

@end
/**
 
 `DataOperation` is a subclass of `ConcurrentOperation` for loading data for specified url
 
 */
@interface DataOperation : ConcurrentOperation <URLDataLoader>

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong, readonly) NSURL *url;
@property (nonatomic, strong, readonly) NSData *data;
@property (nonatomic, strong, readonly) NSError *error;
/**
 
 Initializes an `DataOperation` object with the specified base URL.
 
 @param url The base URL for the opertion.
 
 @return The newly-initialized operation
 
 */
- (instancetype)initWithUrl: (NSURL *) url
            successCAllback: (void(^)(NSData *)) successCallback
              errorCallback: (void(^)(NSError *)) errorCallback;

@end
