//
//  NetworkManager.h
//  SoundCloud
//
//  Created by Andrey Volobuev on 23/02/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataOperation.h"
#import "Item.h"



@interface NetworkManager : NSObject

@property (nonatomic, strong) NSURLSession *session;

/**
 
 Loads data for the specified base URL.
 
 @param url URL to load data.
 
 */

-(void)loadDataForURL: (NSURL *) url
      successCallback: (void(^)(NSData*, NSURL*)) successCallback
        errorCallback: (void(^)(NSError*)) errorCallback;

-(void)cancelAll;

@end
