//
//  ConcurrentOperatin.m
//  FlickrTest
//
//  Created by Andrey Volobuev on 2/21/16.
//  Copyright © 2016 test. All rights reserved.
//

#import "ConcurrentOperation.h"


@interface ConcurrentOperation (){
    BOOL executing;
    BOOL finished;
}

@end

@implementation ConcurrentOperation

- (instancetype)init {
    self = [super init];
    if (self) {
        executing = NO;
        finished = NO;
    }
    return self;
}

-(BOOL)isExecuting {
    return executing;
}

-(BOOL)isFinished {
    return finished;
}

-(void)finishOperation {
    [self willChangeValueForKey:@"isFinished"];
    finished = YES;
    [self didChangeValueForKey:@"isFinished"];
}

-(void)start {
    if([self isCancelled]) {
        [self finishOperation];
        return;
    }
    
    [self willChangeValueForKey:@"isExecuting"];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self main];
}

@end
