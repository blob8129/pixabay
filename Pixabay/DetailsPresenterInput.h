//
//  DetailsPresenterInput.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DetailsPresenterInput <NSObject>

-(void)viewDidLoad;

@end
