//
//  PresenterInput.h
//  Pixabay
//
//  Created by Andrey Volobuev on 15/03/2017.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol PresenterInput <NSObject>

-(void)viewDidLoad;
-(NSInteger)numberOfRows;
-(void)didChangedSearchTerm: (NSString *) searchTerm;
-(NSString *)getInitialSearchTerm;
-(MainViewModel *)viewMOdelForIndex: (NSInteger) index;
-(void)didSelectedIndex: (NSInteger) index;

@end
